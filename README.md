PHP.Skeleton
=======
A standard PHP project skeleton
---------------------------------------------
This project was created in order to provide project skeleton to start new PHP project.

Requirements
---------
 * PHP 5.3+

Getting started
===============

```
 $ composer create-project php/skeleton Vendor.Package
 $ cd Vendor.Package
 $ composer dump-autoload
 $ phpunit
```
