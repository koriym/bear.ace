<?php
/**
 * This file is part of the BEAR.Ace package
 *
 * @package BEAR.Ace
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace BEAR\Ace;

class Exception extends \RuntimeException
{
}